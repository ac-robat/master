#ifndef STEP_DETECTOR_H
#define STEP_DETECTOR_H

#include <stdio.h>
#include <logout.h>

class StepDetector
{
private:
	int iaPastLWheelCount[10];

	int iaPastRWheelCount[10];


public:
	bool DetectRWheelTouch();

	bool DetectLWheelTouch();

};
#endif
