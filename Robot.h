#ifndef ROBOT_H
#define ROBOT_H

#include <stdio.h>
#include <logout.h>

#include "SeesawScenario.h"
#include "LineTraceScenario.h"
#include "GarageScenario.h"
#include "LUGScenario.h"
#include "StartScenario.h"
#include "ScenarioMaster.h"
#include "LineTraceWalker.h"
#include <ev3api.h>

class Robot
{
private:
	int iCoursePhase=0;
    bool bFinish;
    int iLoop=0;

	LineTraceScenario cLineTraceScenario;
	GarageScenario cGarageScenario;
	LUGScenario cLUGScenario;
	StartScenario cStartScenario;
	SeesawScenario cSeesawScenario;
	LineTraceWalker cLineTraceWalker;

public:
	bool AchieveCource();

};
#endif
