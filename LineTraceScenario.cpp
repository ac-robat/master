#include <stdio.h>
#include <logout.h>

#include "ev3api.h"
#include "LineTraceScenario.h"



void LineTraceScenario::JudgePhase()
{
    logout("LineTraceScenario::JudgePhase()");
    sDetectionMaster.GetStatus(&sDetectionResult);
    
	bFlagBlue = sDetectionResult.bFlagBlueLine;
	if(bFlagBlue == true){
		iLtsPhase++;
	}
}

bool LineTraceScenario::Achieve()
{
    logout("LineTraceScenario::Achieve()");
	cLineTraceWalker.SetSpeed(30);     // 10, 30, 60
	cLineTraceWalker.LineTrace();
	if (iLtsPhase == 1){
	    return true;	
	} else {
		return false;
	}

}
