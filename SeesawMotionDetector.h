#ifndef SEESAW_MOTION_DETECTOR_H
#define SEESAW_MOTION_DETECTOR_H

#include <stdio.h>
#include <logout.h>

class SeesawMotionDetector
{
private:
	int iaPastGyroValue[10];


public:
	bool DetectSeesawMotion(int iRunLength);

	bool DetectSeesawStable();

};
#endif
