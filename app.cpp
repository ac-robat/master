#include "logout.h"
#include "app.h"
#include "Robot.h"
#include <stdio.h>

//using namespace ev3api;

Robot cRobot;
bool bFinish = false;


void robot_cyc(intptr_t exinf) {
    act_tsk(ROBOT_TASK);
}


void robot_task(intptr_t exinf) {
  if (bFinish == true) {
    wup_tsk(MAIN_TASK);
  } else {
    bFinish = cRobot.AchieveCource();
      logout("");
  }
  ext_tsk();
}


void main_task(intptr_t unused) {
    logout("main_task");

    
    ev3_sta_cyc(ROBOT_CYC);
    slp_tsk();
    ev3_stp_cyc(ROBOT_CYC);
    
    ev3_lcd_draw_string("Complete",0,100);
    logout("Complete!");
    ext_tsk();//全体の走行終了
}
