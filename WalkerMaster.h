#ifndef WALKER_MASTER_H
#define WALKER_MASTER_H

#include <stdio.h>
#include <logout.h>

#include "DetectionMaster.h"
#include "Balancer.h"
#include "GapControl.h"
#include "PIDControl.h"
#include "ev3api.h"
#include "motor.h"

class WalkerMaster
{
protected:
	int iSpeed;
	PIDControl cPIDControl;
	GapControl cGapControl;
	Balancer cBalancer;
	ev3api::Motor cLMotor;
	ev3api::Motor cRMotor;

public:
	WalkerMaster();
	virtual void LineTrace()=0;

	void NumRun();

	void SetSpeed(int speedVal);

	void Stop();

};
#endif
