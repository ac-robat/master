#ifndef LINE_TRACE_SCENARIO_H
#define LINE_TRACE_SCENARIO_H

#include <stdio.h>
#include <logout.h>

#include "ev3api.h"
#include "ScenarioMaster.h"
#include "LineTraceWalker.h"

class LineTraceScenario : public ScenarioMaster
{
private:
	LineTraceWalker cLineTraceWalker;
	int iLtsPhase = 0;
	bool bFlagBlue = false;
	
public:
	void JudgePhase();
	bool Achieve();
	DetectionMaster sDetectionMaster;

};
#endif
