#ifndef SEESAW_WALKER_H
#define SEESAW_WALKER_H

#include <stdio.h>
#include <logout.h>

#include "WalkerMaster.h"

class SeesawWalker : public WalkerMaster
{
public:
	void LineTrace();

	void LineTrace(bool bFlagLTouch, bool bFlagRTouch);

};
#endif
