#ifndef LINE_TRACE_WALKER_H
#define LINE_TRACE_WALKER_H

#include <stdio.h>
#include <logout.h>
#include "Motor.h"
#include "ColorSensor.h"

#include "WalkerMaster.h"

class LineTraceWalker : public WalkerMaster
{
public:
	LineTraceWalker();
	void LineTrace();

private:
	ev3api::Motor cLMotor;
	ev3api::Motor cRMotor;
	ev3api::ColorSensor cColorSensor;

};
#endif
