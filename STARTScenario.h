#ifndef START_SCENARIO_H
#define START_SCENARIO_H

#include <stdio.h>
#include <logout.h>

#include "ev3api.h"
#include "Clock.h"
#include "ScenarioMaster.h"
#include "StartWalker.h"
#include "Motor.h"
#include "GyroSensor.h"
#include "TouchSensor.h"
#include "ColorSensor.h"
#include "Sensor.h"
#include "Balancer.h"

class StartScenario : public ScenarioMaster
{
private:

	//StartWalker cStartWalker;
	//StartWalker cScenarioMaster;

	Balancer cBalancer;

	ev3api::Motor cRMotor;
	ev3api::Motor cLMotor;
	ev3api::Motor cTailMotor;
	ev3api::Clock cClock;
	ev3api::TouchSensor cTouchSensor;
	ev3api::ColorSensor cColorSensor;
	ev3api::GyroSensor cGyroSensor;


public:
	StartScenario();

	void JudgePhase();
	bool Achieve();


};
#endif
