#ifndef DETECTION_MASTER_H
#define DETECTION_MASTER_H

#include <stdio.h>
#include <logout.h>

#include "SeesawMotionDetector.h"
#include "StepDetector.h"
#include "LineDetector.h"
#include "RunLengthCalculator.h"
#include "LUGDetector.h"
#include "PositionDetector.h"
#include "DetectionResult.h"

class DetectionMaster
{
private:
	SeesawMotionDetector cSeesawMotionDetector;
	RunLengthCalculator cRunLengthCalculater;
	LineDetector cLineDetector;
	StepDetector cStepDetecter;
	PositionDetector cPositionDetecter;
	DetectionResult sDetectionResult;

private:
	void CalcStatus();

public:
	void GetStatus(DetectionResult* sDetectionResult);

	void ResetFlag();

};
#endif
