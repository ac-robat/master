#ifndef BALANCER_H
#define BALANCER_H

#include <stdio.h>
#include <logout.h>

#include "ev3api.h"
#include <GyroSensor.h>
#include <Motor.h>

class Balancer
{
public:
	Balancer();

	void Initialize(int iOffset);
	void CalcPWM(float args_cmd_forward, float args_cmd_turn, signed char *ret_pwm_l, signed char *ret_pwm_r);

private:
	float args_cmd_forward;
	float args_cmd_turn;
	float args_gyro;
	float args_gyro_offset;
	float args_theta_m_l;
	float args_theta_m_r;
	// int8_t ret_pwm_l;
	// int8_t ret_pwm_r; 
	ev3api::GyroSensor cGyroSensor;
	ev3api::Motor cMotor_l;
	ev3api::Motor cMotor_r;

};


#endif
