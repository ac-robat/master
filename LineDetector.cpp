#include <stdio.h>
#include <logout.h>

#include "LineDetector.h"


LineDetector::LineDetector() : cColorSensor(PORT_3){
    PastColor[3] = {};
}

bool LineDetector::DetectBlueLine()
{
    iIndex++;
    int BlueSum  = 0;
    int iPoint = iIndex % 3; 

    cColorSensor.getRawColor(PastColor[iPoint]);

    for(int i = 0; i<3; i++){
        BlueSum = BlueSum + PastColor[i].b - PastColor[i].r;
    }
    
    if (BlueSum > 120 * 3) {
        return true;
    }
    else {
        return false;
    }
}
