mkfile_path := $(dir $(lastword $(MAKEFILE_LIST)))

APPL_COBJS += \
 balancer_basic.o   \
 balancer_param_basic.o

APPL_CXXOBJS += \
 Balancer.o   \
 DetectionMaster.o   \
 GapControl.o   \
 GarageScenario.o   \
 GarageWalker.o   \
 LineDetector.o   \
 LineTraceScenario.o   \
 LineTraceWalker.o   \
 LUGDetector.o   \
 LUGScenario.o   \
 LUGWalker.o   \
 logout.o   \
 PIDControl.o   \
 PositionControl.o   \
 PositionDetector.o   \
 Robot.o   \
 RunLengthCalculator.o   \
 ScenarioMaster.o   \
 SeesawMotionDetector.o   \
 SeesawScenario.o   \
 SeesawWalker.o   \
 StartScenario.o   \
 StartWalker.o   \
 StepDetector.o   \
 WalkerMaster.o


SRCLANG := c++

ifdef CONFIG_EV3RT_APPLICATION

# Include libraries
include $(EV3RT_SDK_LIB_DIR)/libcpp-ev3/Makefile

endif

APPL_DIR += \
-I$(mkfile_path)Action\
-I$(mkfile_path)Detect\
-I$(mkfile_path)Scenario

INCLUDES += \
-I$(mkfile_path)Action\
-I$(mkfile_path)Detect\
-I$(mkfile_path)Scenario

COPTS += -fno-use-cxa-atexit
