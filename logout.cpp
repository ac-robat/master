//
//  logout.cpp
//  ACROBAT
//
//  Created by Akihito Yoshito on 2019/07/23.
//

#include "logout.h"

void logout(const char *cIn){
    if(LOGOUT){
        FILE *fp;
        fp = fopen("ACROBATLog.txt","a");
        fputs(cIn,fp);
        fputs("\n",fp);
        fclose(fp);
    }
    
}

void logoutI(int iIn){
    if(LOGOUT){
        FILE *fp;
        fp = fopen("ACROBATLog.txt","a");
        fprintf(fp,"%d\n",iIn);
        fclose(fp);
    }
    
}

void logoutF(float fIn){
    if(LOGOUT){
        FILE *fp;
        fp = fopen("ACROBATLog.txt","a");
        fprintf(fp,"%.4f\n",fIn);
        fclose(fp);
    }
    
}

void logoutB(bool bIn){
    if(LOGOUT){
        FILE *fp;
        fp = fopen("ACROBATLog.txt","a");
        fprintf(fp,"%s\n",(bIn ? "true" : "false"));
        fclose(fp);
    }
    
}
