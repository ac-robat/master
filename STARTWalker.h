#ifndef START_WALKER_H
#define START_WALKER_H

#include <stdio.h>
#include <logout.h>

#include "WalkerMaster.h"

class StartWalker : public WalkerMaster
{
public:
	void LineTrace();

};
#endif
