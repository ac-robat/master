#include <stdio.h>
#include <logout.h>

#include "DetectionMaster.h"


void DetectionMaster::CalcStatus()
{
	logout("DetectionMaster::CalcStatus()");
	sDetectionResult.bFlagBlueLine = cLineDetector.DetectBlueLine();
}

void DetectionMaster::GetStatus(DetectionResult* sInDetectionResult)
{
    logout("DetectionMaster::GetStatus()");
	CalcStatus();
	sInDetectionResult->bFlagBlueLine = sDetectionResult.bFlagBlueLine;
}

void DetectionMaster::ResetFlag()
{
}
