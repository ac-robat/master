#include <stdio.h>
#include <logout.h>

#include "Robot.h"


bool Robot::AchieveCource()
{
    iLoop++;
    logout("Robot::AchieveCource()");
    switch (iCoursePhase) {
        case (0):
            bFinish = cStartScenario.Achieve();
            if (bFinish == true){
                iCoursePhase = 1;
                bFinish = false;
            }
            return false;
            
        case (1):
            bFinish = cLineTraceScenario.Achieve();
            if (iLoop > 1000){
                iCoursePhase = 2;
                bFinish = false;
            }
            logout("");
            return false;

        case (2):
            bFinish = cLineTraceScenario.Achieve();
            // cLineTraceScenario.JudgePhase();
            if (bFinish == true){
                iCoursePhase = 9;
                bFinish = false;
            }
            logout("");
            return false;
            
        case (9):
			cLineTraceWalker.Stop();
            return true;
    }
    return false;
}
