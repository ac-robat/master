//
//  logout.hpp
//  ACROBAT
//
//  Created by Akihito Yoshito on 2019/07/23.
//

#ifndef logout_h
#define logout_h

#define LOGOUT 0
#include <stdio.h>
#include <stdbool.h>


extern void logout(const char *cIn);
extern void logoutI(int iIn);
extern void logoutF(float fIn);
extern void logoutB(bool bIn);


#endif /* logout_h */
