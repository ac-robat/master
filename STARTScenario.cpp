#include <stdio.h>
#include <logout.h>

#include "StartScenario.h"
#include "ev3api.h"


StartScenario::StartScenario() :cRMotor(PORT_B,false,LARGE_MOTOR),
								cLMotor(PORT_C, false, LARGE_MOTOR),
								cTailMotor(PORT_A, false, LARGE_MOTOR),
								cTouchSensor(PORT_1),
								cColorSensor(PORT_3),
								cGyroSensor(PORT_4)
{

}
void StartScenario::JudgePhase()
{
}

bool StartScenario::Achieve()
{

	logout("StartScenario::Achieve()");
	ev3_motor_reset_counts(EV3_PORT_A);
	ev3_led_set_color(LED_RED);

	//signed char forward;      // 前後進命令
	//signed char turn;         // 旋回命令
	//signed char pwm_L, pwm_R; // 左右モーターPWM出力

	while (1)
	{
		/* 完全停止用角度に制御 (85°) */
		signed int pwm = (signed int)((83 - ev3_motor_get_counts(EV3_PORT_A)) * 2.5F); /* 比例制御 */
		/* PWM出力飽和処理 */
		if (pwm > 60)
		{
			pwm = 60;
		}
		else if (pwm < -60)
		{
			pwm = -60;
		}
		if (pwm == 0)
		{
			ev3_motor_stop(EV3_PORT_A, true);
		}
		else
		{
			ev3_motor_set_power(EV3_PORT_A, (signed char)pwm/2);
		};

		//if (bt_cmd == 1){
		//	break; // リモートスタートはまだ未搭載
        //}

		if (ev3_touch_sensor_is_pressed(EV3_PORT_1) == true)
		{
            ev3_lcd_draw_string("Touched", 0, 80);
			break; // タッチセンサーが押された
		}
		

	}
	ev3_motor_reset_counts(EV3_PORT_C);
	ev3_motor_reset_counts(EV3_PORT_B);
	ev3_led_set_color(LED_ORANGE);

	/* ジャイロセンサーリセット */
	ev3_gyro_sensor_reset(EV3_PORT_4);
	cBalancer.Initialize(0);
    
	while (1)
	{
		//int motor_ang_l, motor_ang_r;
		//int gyro, volt;

		//if (ev3_button_is_pressed(BACK_BUTTON)) break;

		// バランス走行用角度に制御
		signed int pwm = (signed int)((3 - ev3_motor_get_counts(EV3_PORT_A)) * 2.5F); // 比例制御 
		// PWM出力飽和処理
		if (pwm > 60)
		{
			pwm = 60;
		}
		else if (pwm < -60)
		{
			pwm = -60;
		}
		if (pwm == 0)
		{
			ev3_motor_stop(EV3_PORT_A, true);
		}
		else
		{
			ev3_motor_set_power(EV3_PORT_A, (signed char)pwm);
		};

        /*
		forward = 30; // 前進命令
		if (ev3_color_sensor_get_reflect(EV3_PORT_3) >= (55 + 0) / 2)
		{
			turn = 20; // 右旋回命令
		}
		else
		{
			turn = -20; // 左旋回命令
		}
         */


		/*
	//	 倒立振子制御API に渡すパラメータを取得する
		motor_ang_l = ev3_motor_get_counts(EV3_PORT_C);
		motor_ang_r = ev3_motor_get_counts(EV3_PORT_B);
		gyro = ev3_gyro_sensor_get_rate(EV3_PORT_4);
		volt = ev3_battery_voltage_mV();
		*/
		return true;
	}
    
}

