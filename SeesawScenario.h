#ifndef SEESAW_SCENARIO_H
#define SEESAW_SCENARIO_H

#include <stdio.h>
#include <logout.h>

#include "ScenarioMaster.h"
#include "SeesawWalker.h"
#include "PositionControl.h"
#include "DetectionMaster.h"

class SeesawScenario : public ScenarioMaster
{
private:
	PositionControl cPositionControl;
	DetectionMaster cDetectionMaster;
	SeesawWalker cSeesawWalker;

public:
	void JudgePhase();

	bool Achieve();

};
#endif
