#ifndef LINE_DETECTOR_H
#define LINE_DETECTOR_H

#include <stdio.h>
#include "logout.h"
#include "ev3api.h"
#include "ColorSensor.h"

class LineDetector
{
private:
    ev3api::ColorSensor cColorSensor;
    rgb_raw_t PastColor[3];
    int iIndex = 0;
    
public:
    LineDetector();
	bool DetectBlueLine();

};
#endif
