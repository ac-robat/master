#ifndef SCENARIO_MASTER_H
#define SCENARIO_MASTER_H

#include <stdio.h>
#include <logout.h>

#include "DetectionMaster.h"
#include "PositionControl.h"
#include "DetectionResult.h"
#include "WalkerMaster.h"

class ScenarioMaster
{
protected:
	bool bFlagScenarioComplete;

	int iPhase;
    
	PositionControl cPositionControl;
	DetectionMaster cDetectionMaster;
	PositionControl positionControl;
	DetectionResult sDetectionResult;

public:
	virtual void JudgePhase()=0;

	virtual bool Achieve()=0;

};
#endif
