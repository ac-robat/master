#include <stdio.h>
#include <logout.h>


#include "Balancer.h" //サンプルコードでいうところのBalancerCpp.h
#include "balancer_basic.h"

// コンストラクタ
Balancer::Balancer()
    : cGyroSensor(PORT_4),
      cMotor_l(PORT_C, false, LARGE_MOTOR),
      cMotor_r(PORT_B, false, LARGE_MOTOR) {
}


void Balancer::Initialize(int iOffset)
{
    balance_init();  // 倒立振子制御初期化
}

void Balancer::CalcPWM(float args_cmd_forward, float args_cmd_turn, signed char *ret_pwm_l, signed char *ret_pwm_r)
{
	float args_gyro = 0.0;
	float args_gyro_offset = -5.0; // -15, -5, 5, 10, 15
	float args_theta_m_l = 0.0;
	float args_theta_m_r = 0.0;
	float args_battery = 0.0;
	const int kBacklashHalf = 4;  // バックラッシュの半分[deg]

    if (ret_pwm_l == 0); 
	else ret_pwm_l > 0 ? (*ret_pwm_l - kBacklashHalf) : (*ret_pwm_l + kBacklashHalf);
	
    if (ret_pwm_r == 0); 
	else ret_pwm_r > 0 ? (*ret_pwm_r - kBacklashHalf) : (*ret_pwm_r + kBacklashHalf);


	args_gyro = static_cast<float>(cGyroSensor.getAnglerVelocity());
	// args_gyro_offset = static_cast<float>();
	args_theta_m_l = static_cast<float>(cMotor_l.getCount());
	args_theta_m_r = static_cast<float>(cMotor_r.getCount());
	args_battery = static_cast<float>(ev3_battery_voltage_mV());


	balance_control(
        args_cmd_forward,
        args_cmd_turn,
        args_gyro,
        args_gyro_offset,
        args_theta_m_l,
        args_theta_m_r,
        args_battery,
        ret_pwm_l,
        ret_pwm_r);

}
