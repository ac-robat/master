#ifndef POSITION_CONTROL_H
#define POSITION_CONTROL_H

#include <stdio.h>
#include <logout.h>

class PositionControl
{
private:
	static int iTailAngle;


public:
	void UseTail();

	void AntiImpact();

};
#endif
