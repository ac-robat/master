#ifndef GARAGE_SCENARIO_H
#define GARAGE_SCENARIO_H

#include <stdio.h>
#include <logout.h>

#include "ScenarioMaster.h"
#include "GarageWalker.h"

class GarageScenario : public ScenarioMaster
{
private:
	GarageWalker cGarageWalker;

public:
	void JudgePhase();

	bool Achieve();

};
#endif
