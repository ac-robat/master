﻿#include <stdio.h>
#include <logout.h>
#include "ev3api.h"
#include "RunLengthCalculator.h"
#include "math.h"

#define TIRE_RADIOUS 0.5

RunLengthCalculator::RunLengthCalculator()
	:	

CalcRunLength(ev3api::Motor& mLeftWheel, ev3api::Motor& mRightWheel)
{
	:mLeftWheel(leftWheel),
	mRightWheel(rightWheel){ }
}

int RunLengthCalculator::CalcRunLength()
{
	int rightWheelEnc = mRightWheel.getCount();
	int leftWheelEnc = mLeftWheel.getCount();
	double robotVelocity = 0;
	double robotRoundVelocity = 0;
	double xRunLengthPerSec = 0;
	double yRunLengthPerSec = 0;
	double runLengthPerSec = 0;
	
	iaPastRWheelCount.push_back(rightWheelEnc);
	iaPastLWheelCount.push_back(leftWheelEnc);

	/*　Convert [deg] to [deg/rad] by using 	following fomula. t\ 
	"wheelEncount ÷ interval of CalcRunLength function call."
	*/

	robotVelocity = ( (rightWheelEnc + leftWheelEnc) * TIRE_RADIOUS) / 2; /*Robot velocity
	*/
	robotRoundVelocity = sqrt( rightWheelEnc - leftWheelEnc); 

	xRunLengthPerSec = robotRoundVelocity * cos(rightWheelEnc);
	yRunLengthPerSec = robotRoundVelocity * sin(rightWheelEnc);
	runLengthPerSec = sqrt( pow(xRunLengthPerSec, 2) + pow(yRunLengthPerSec);
	
	iTotalRunLength = +iRunLength;

	return iRunLength;
}

void RunLengthCalculator::ResetRunLength()
{
	iRunLength = 0;
//	iaPastLWheelCount[10] = { };
//	iaPastRWheelCount[10] = { };
	mLeftWheel.reset();
	mRightWheel.reset();

}