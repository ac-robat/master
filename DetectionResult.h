#ifndef DETECTION_RESULT_H
#define DETECTION_RESULT_H

#include <stdio.h>
#include <logout.h>

struct DetectionResult
{
public:
	bool bFlagLTouch;

	bool bFlagRTouch;

	bool bFlagSeesawMotion;

	bool bFlagSeesawStable;

	bool bFlagBlueLine = false;

	bool bFlagLUG;

	int iRunLength;

	bool bFlagStable;

	int iRobotAngle;

};
#endif
