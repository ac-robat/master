#ifndef L_U_G_SCENARIO_H
#define L_U_G_SCENARIO_H

#include <stdio.h>
#include <logout.h>

#include "ScenarioMaster.h"
#include "PositionControl.h"
#include "LUGWalker.h"

class LUGScenario : public ScenarioMaster
{
private:
	PositionControl cPositionControl;
	LUGWalker cLUGWalker;

public:
	void JudgePhase();

	bool Achieve();

};
#endif
