#ifndef RUN_LENGTH_CALCULATOR_H
#define RUN_LENGTH_CALCULATOR_H

#include <vector>
#include <stdio.h>
#include <logout.h>
#include "ev3api.h"
#include "Motor.h"
class RunLengthCalculator
{
private:
	std::vector<int> iaPastRWheelCount(10);
	std::vector<int> iaPastLWheelCount(10);

//	int iaPastRWheelCount[10];

	int iTotalRunLength;

	int iRunLength;
	
	ev3api::Motor& mLeftWheel;
	ev3api::Motor& mRightWheel;


public:
	int CalcRunLength();

	void ResetRunLength();

	CalcRunLength(ev3api::Motor& leftWheel, ev3api::Motor& rightWheel);
	

};
#endif