#ifndef POSITION_DETECTOR_H
#define POSITION_DETECTOR_H

#include <stdio.h>
#include <logout.h>

class PositionDetector
{
private:
	int iaPastGyroValue[10];

	int iaPastRobotAngle[10];


public:
	int CalcRobotAngle();

	bool DetectStable();

};
#endif
